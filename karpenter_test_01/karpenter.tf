################################################################################
# Karpenter
################################################################################
module "karpenter" {
  source = "terraform-aws-modules/eks/aws//modules/karpenter"

  cluster_name = module.eks.cluster_name

  irsa_oidc_provider_arn          = module.eks.oidc_provider_arn
  irsa_namespace_service_accounts = ["karpenter:karpenter"]

  enable_karpenter_instance_profile_creation = true

  # Used to attach additional IAM policies to the Karpenter node IAM role
  iam_role_additional_policies = {
    AmazonSSMManagedInstanceCore = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  }

  # create_iam_role = false
  # iam_role_arn    = module.eks.eks_managed_node_groups["initial"].iam_role_arn

  tags = {
    Environment = "dev"
    Terraform   = "true"
  }
}

resource "helm_release" "karpenter" {
  namespace        = "karpenter"
  create_namespace = true

  name                = "karpenter"
  repository          = "oci://public.ecr.aws/karpenter"
  repository_username = data.aws_ecrpublic_authorization_token.token.user_name
  repository_password = data.aws_ecrpublic_authorization_token.token.password
  chart               = "karpenter"
  version             = "v0.33.0"

  values = [
    <<-EOT
    settings:
      clusterName: ${module.eks.cluster_name}
      clusterEndpoint: ${module.eks.cluster_endpoint}
      interruptionQueueName: ${module.karpenter.queue_name}
    serviceAccount:
      annotations:
        eks.amazonaws.com/role-arn: ${module.karpenter.irsa_arn} 
    affinity:
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: karpenter.sh/nodepool
              operator: DoesNotExist
          - matchExpressions:
            - key: eks.amazonaws.com/nodegroup
              operator: In
              values:
              - karpenter_managed_nodegroup

      podAntiAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          - topologyKey: "kubernetes.io/hostname"
    tolerations:
      - key: "karpenter-system-node"
        operator: "Exists"
        effect: "NoSchedule"
    EOT
  ]
}

resource "kubectl_manifest" "karpenter_node_class" {
  yaml_body = <<-YAML
    apiVersion: karpenter.k8s.aws/v1beta1
    kind: EC2NodeClass
    metadata:
      name: default
    spec:
      amiFamily: AL2
      role: ${module.karpenter.role_name}
      subnetSelectorTerms:
        - tags:
            karpenter.sh/discovery: ${module.eks.cluster_name}
      securityGroupSelectorTerms:
        - tags:
            karpenter.sh/discovery: ${module.eks.cluster_name}
      tags:
        karpenter.sh/discovery: ${module.eks.cluster_name}
      
      blockDeviceMappings:
        - deviceName: /dev/xvda
          ebs:
            volumeSize: 20Gi
            volumeType: gp3
            iops: 3000
            encrypted: true
            deleteOnTermination: true
            throughput: 125
     
  YAML

  depends_on = [
    helm_release.karpenter
  ]
}

resource "kubectl_manifest" "karpenter_node_pool" {
  yaml_body = <<-YAML
    apiVersion: karpenter.sh/v1beta1
    kind: NodePool
    metadata:
      name: default
    spec:
      template:
        spec:
          nodeClassRef:
            name: default
          requirements:
            - key: node.kubernetes.io/instance-type
              operator: In
              values: [t3.small, t3.medium]
            # Exclude small instance sizes
            - key: node.kubernetes.io/instance-type
              operator: NotIn
              values: [t3.micro, t3.nano]  
            - key: "karpenter.k8s.aws/instance-hypervisor"
              operator: In
              values: ["nitro"]
            - key: "topology.kubernetes.io/zone"
              operator: In
              values: ${jsonencode(local.azs)}
            - key: "kubernetes.io/arch"
              operator: In
              values: ["amd64"]
            - key: "karpenter.sh/capacity-type" # If not included, the webhook for the AWS cloud provider will default to on-demand
              operator: In
              values: ["on-demand"]

          kubelet:
            maxPods: 110

      disruption:
          consolidationPolicy: WhenEmpty
          consolidateAfter: 30s
          expireAfter: 168h

      limits:
        cpu: 10
      
      weight: 1
    
  YAML

  depends_on = [
    helm_release.karpenter
  ]
}

### Karpenter EKS Managed Node Group BLOCK START ###
locals {
  # cluster_version = "1.28"
  iam_policy_arn = [
    "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
    "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  ]

}

### Karpenter IAM role - BLOCK START
resource "aws_iam_role" "karpenter_managed_nodegroup_role" {
  name = "karpenter_managed_nodegroup_role"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })

  tags = {
    "karpenter.sh/discovery" = module.eks.cluster_name
  }
}

resource "aws_iam_role_policy_attachment" "karpenter_managed_nodegroup-policy_attachment" {
  for_each   = toset(local.iam_policy_arn)
  policy_arn = each.value
  role       = aws_iam_role.karpenter_managed_nodegroup_role.name
}

### Karpenter IAM role - BLOCK END

### Karpenter Launch template - BLOCK START
resource "aws_launch_template" "karpenter_launch_template" {
  name = "karpenter-launch-template"

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size           = 20
      delete_on_termination = true
      volume_type           = "gp3"
      encrypted             = true
      iops                  = 3000
      throughput            = 125
    }
  }
  instance_type = "t3.small"

  credit_specification {
    cpu_credits = "unlimited"
  }

  ebs_optimized = true

  vpc_security_group_ids = [
    module.eks.cluster_primary_security_group_id,
    module.eks.node_security_group_id
  ]

  monitoring {
    enabled = true
  }

  #   user_data = base64encode(<<EOF
  # MIME-Version: 1.0
  # Content-Type: multipart/mixed; boundary="==MYBOUNDARY=="

  # --==MYBOUNDARY==
  # Content-Type: text/x-shellscript; charset="us-ascii"

  # #!/bin/bash
  # set -ex
  # /etc/eks/bootstrap.sh "${var.cluster_name}" \
  #   --kubelet-extra-args '--max-pods=50' \
  #   --use-max-pods false

  # --==MYBOUNDARY==--
  #   EOF
  #   )


  tag_specifications {
    resource_type = "instance"

    tags = {
      Name                     = "${module.eks.cluster_name}-karpenter-launch-template"
      "karpenter.sh/discovery" = module.eks.cluster_name
    }

  }
}
### Karpenter Launch template - BLOCK END

### EKS managed node group for karpenter - BLOCK START
data "aws_ssm_parameter" "eks_ami_release_version" {
  name = "/aws/service/eks/optimized-ami/${local.cluster_version}/amazon-linux-2/recommended/release_version"
}

resource "aws_eks_node_group" "karpenter_managed_nodegroup" {
  cluster_name    = module.eks.cluster_name
  node_group_name = "${module.eks.cluster_name}-karpenter_managed_nodegroup"
  release_version = nonsensitive(data.aws_ssm_parameter.eks_ami_release_version.value)
  node_role_arn   = aws_iam_role.karpenter_managed_nodegroup_role.arn
  subnet_ids      = module.vpc.private_subnets
  # instance_types  = var.karpenter_managed_nodegroup_instance_type
  ami_type      = "AL2_x86_64"
  capacity_type = "ON_DEMAND"
  # disk_size       = 40
  launch_template {
    id      = aws_launch_template.karpenter_launch_template.id
    version = "$Latest"
  }
  labels = {
    managed_by = "karpenter-managed-nodegroup"
  }
  taint {
    key    = "karpenter-system-node"
    value  = "on-demand"
    effect = "NO_SCHEDULE"

  }


  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }

  update_config {
    max_unavailable = 1
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.karpenter_managed_nodegroup-policy_attachment,
    # data.aws_eks_cluster.cluster
  ]

  tags = {
    "karpenter.sh/discovery" = module.eks.cluster_name
  }

  lifecycle {
    create_before_destroy = true
  }
}
### EKS managed node group for karpenter - BLOCK END

### Karpenter EKS Managed Node Group BLOCK END ###
