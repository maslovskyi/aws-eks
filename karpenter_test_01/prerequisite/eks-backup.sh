#!/bin/bash

# Specify your AWS region EX: us-east-2
AWS_REGION="us-east-1"

# Specify your Cluster name EX: prod-cluster
CLUSTER_NAME="ascaler_vs_karpenter"

#Env EX: prod-1-23
ENV_VARSION="prod-1-23"

# List of instance IDs EX: "i-068a" "i-068b" "i-04b"
INSTANCE_IDS=("i-00e102050f97272e9","i-09a5a4ed94f0c2623" "i-09c55f22f9589bec2")

# Describe EKS cluster and save to YAML file
aws eks describe-cluster --name $CLUSTER_NAME --region $AWS_REGION --query "cluster" --output yaml >eks-cluster-config-$ENV_VARSION.yaml
echo "$CLUSTER_NAME cluster info exported"

# Copy EKS cluster config file to S3 EX: s3://eks-backup/eks-backups-prod/$CLUSTER_NAME/
aws s3 cp eks-cluster-config-$ENV_VARSION.yaml s3://tf-s3-backend-11333/cluster/

# List EKS worker node groups and save to YAML file
aws eks list-nodegroups --cluster-name $CLUSTER_NAME --region $AWS_REGION --query "nodegroups" --output yaml >eks-worker-nodes-$ENV_VARSION.yaml
echo "$CLUSTER_NAME worker info exported"

# Copy EKS worker node groups file to S3 s3://eks-backup/eks-backups-prod/$CLUSTER_NAME/
aws s3 cp eks-worker-nodes-$ENV_VARSION.yaml s3://tf-s3-backend-11333/cluster/

# Describe EBS volumes attached to specified instances and save to YAML file
aws ec2 describe-volumes --filters Name=attachment.instance-id,Values=$INSTANCE_IDS --query "Volumes" --output yaml >eks-ebs-volumes-$ENV_VARSION.yaml
echo "$CLUSTER_NAME EBS info exported"

# Copy EBS volumes file to S3 EX: s3://eks-backup/eks-backups-prod/$CLUSTER_NAME/
aws s3 cp eks-ebs-volumes-$ENV_VARSION.yaml s3://tf-s3-backend-11333/cluster/
