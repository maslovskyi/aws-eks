################################################################################
# Kube-prometheus-stack
################################################################################
resource "helm_release" "kube-prometheus" {
  namespace        = "monitoring"
  create_namespace = true

  name       = "kube-prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  version    = "v48.1.1"
  values     = [file("values/prometheus.yaml")]

  set {
    name  = "kubeProxy.enabled"
    value = false
  }

  set {
    name  = "grafana.adminPassword"
    value = aws_secretsmanager_secret_version.grafana.secret_string
  }
}

#---------------------------------------------------------------
# Grafana Admin Password credentials with Secrets Manager
#---------------------------------------------------------------
resource "random_password" "grafana" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "aws_secretsmanager_secret" "grafana" {
  name                    = "grafana"
  recovery_window_in_days = 0 # Set to zero for this example to force delete during Terraform destroy
}

resource "aws_secretsmanager_secret_version" "grafana" {
  secret_id     = aws_secretsmanager_secret.grafana.id
  secret_string = random_password.grafana.result
}
