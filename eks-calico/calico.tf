################################################################################
# Calico tiger operator
################################################################################
resource "helm_release" "calico" {
  namespace        = "tigera-operator"
  create_namespace = true

  name       = "calico"
  repository = "https://docs.tigera.io/calico/charts"
  chart      = "tigera-operator"
  version    = "v3.26.1"
  # values     = [file("values/calico.yaml")]

  set {
    name  = "kubernetesProvider"
    value = "EKS"
  }
}
