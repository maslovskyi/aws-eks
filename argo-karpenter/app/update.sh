#!/bin/bash

# exit when any command fails
set -e

new_ver=$1

echo "new version: $new_ver"

# Simulate release of the new docker images
docker tag nginx shatten/nginx:"$new_ver"

# Push new version to dockerhub
docker push shatten/nginx:"$new_ver"

# Create temporary folder
tmp_dir=$(mktemp -d)
echo "$tmp_dir"

# Clone GitHub repo
git clone git@github.com:amaslovskyi/Argocd.git "$tmp_dir"

# Update image tag
sed -i "s/shatten\/nginx:.*/shatten\/nginx:$new_ver/g" "$tmp_dir/my-app/deploy.yaml"

# Commit and push
cd "$tmp_dir"
git add .
git commit -m "upd: image_to_$new_ver"
git push

# Optionally on build agents - remove folder
rm -rf "$tmp_dir"
