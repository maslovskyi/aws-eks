################################################################################
# Keda manifest file
################################################################################
resource "helm_release" "keda" {
  namespace        = "keda"
  create_namespace = true

  name       = "keda"
  repository = "https://kedacore.github.io/charts"
  chart      = "keda"
  version    = "v2.11.1"
  values     = [file("values/keda.yaml")]
}
