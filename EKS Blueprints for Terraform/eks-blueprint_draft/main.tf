provider "aws" {
  region = local.region
}

# Required for public ECR where Karpenter artifacts are hosted
provider "aws" {
  region = "us-east-1"
  alias  = "virginia"
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "aws"
    # This requires the awscli to be installed locally where Terraform is executed
    args = ["eks", "get-token", "--cluster-name", module.eks.cluster_name]
  }
}

provider "helm" {
  kubernetes {
    host                   = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)

    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "aws"
      # This requires the awscli to be installed locally where Terraform is executed
      args = ["eks", "get-token", "--cluster-name", module.eks.cluster_name]
    }
  }
}

data "aws_ecrpublic_authorization_token" "token" {
  provider = aws.virginia
}

data "aws_availability_zones" "available" {}

locals {
  #  inject the last name in the path for a cluster name
  name     = basename(path.cwd)
  region   = var.region
  vpc_cidr = var.cidr
  azs      = var.azs
  # azs      = slice(data.aws_availability_zones.available.names, 0, 3)

  tags = {
    Blueprint  = local.name
    GithubRepo = "github.com/aws-ia/terraform-aws-eks-blueprints"
  }
}

################################################################################
# Cluster
################################################################################

#tfsec:ignore:aws-eks-enable-control-plane-logging
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 19.15.3"

  cluster_name                   = local.name
  cluster_version                = "1.27"
  enable_irsa                    = true
  cluster_endpoint_public_access = true

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      before_compute = true
      most_recent    = true
      addon_name     = "vpc-cni"
      #addon_version            = "v1.11.2-eksbuild.1"
      service_account          = "aws-node"
      resolve_conflicts        = "OVERWRITE"
      namespace                = "kube-system"
      service_account_role_arn = ""
      preserve                 = true
      additional_iam_policies  = []
      configuration_values = jsonencode({
        env = {
          ENABLE_PREFIX_DELEGATION = "true"
          WARM_PREFIX_TARGET       = "1"
          WARM_IP_TARGET           = "5"
          MINIMUM_IP_TARGET        = "2"
        }
      })
    }
    aws-ebs-csi-driver = {}
  }
  eks_managed_node_groups = {
    initial = {
      instance_types = var.instance_types

      min_size      = 1
      max_size      = 5
      desired_size  = 3
      ebs_optimized = true
      # volumeSize   = 30
      disk_size          = 30
      enable_monitoring  = true
      use_max_pods       = false
      kubelet_extra_args = "--max-pods=110"
    }
  }
  manage_aws_auth_configmap = true
  aws_auth_roles = [
    # We need to add in the Karpenter node IAM role for nodes launched by Karpenter
    {
      # rolearn  = module.eks_blueprints_addons.karpenter.node_iam_role_arn
      rolearn  = module.karpenter.role_arn
      username = "system:node:{{EC2PrivateDNSName}}"
      groups = [
        "system:bootstrappers",
        "system:nodes",
      ]
    },
  ]

  tags = merge(local.tags, {
    # NOTE - if creating multiple security groups with this module, only tag the
    # security group that Karpenter should utilize with the following tag
    # (i.e. - at most, only one security group should have this tag in your account)
    "karpenter.sh/discovery" = local.name
  })
}

################################################################################
# EKS Blueprints Addons
################################################################################

module "eks_blueprints_addons" {
  # source = "github.com/aws-ia/terraform-aws-eks-blueprints//modules/kubernetes-addons?ref=v4.32.1"

  source  = "aws-ia/eks-blueprints-addons/aws"
  version = "~> 1.1.0"

  cluster_name      = module.eks.cluster_name
  cluster_endpoint  = module.eks.cluster_endpoint
  cluster_version   = module.eks.cluster_version
  oidc_provider_arn = module.eks.oidc_provider_arn

  # eks_cluster_id        = module.eks.cluster_name
  # eks_cluster_endpoint  = module.eks.cluster_endpoint
  # eks_cluster_version   = module.eks.cluster_version
  # eks_oidc_provider     = module.eks.oidc_provider
  # eks_oidc_provider_arn = module.eks.oidc_provider_arn

  # EKS Addons
  # eks_addons = {
  #   coredns = {}
  #   vpc-cni = {
  #     addon_name = "vpc-cni"
  #     before_compute = true
  #     #addon_version            = "v1.11.2-eksbuild.1"
  #     service_account          = "aws-node"
  #     resolve_conflicts        = "OVERWRITE"
  #     namespace                = "kube-system"
  #     service_account_role_arn = ""
  #     preserve                 = true
  #     additional_iam_policies  = []
  #     configuration_values = jsonencode({
  #       env = {
  #         ENABLE_PREFIX_DELEGATION = "true"
  #         WARM_PREFIX_TARGET       = "1"
  #         WARM_IP_TARGET           = "5"
  #         MINIMUM_IP_TARGET        = "2"
  #       }
  #     })
  #   }
  #   kube-proxy         = {}
  #   aws-ebs-csi-driver = {}
  # }
  # enable_amazon_eks_aws_ebs_csi_driver = true
  # enable_karpenter = true
  # enable_amazon_eks_coredns            = true
  # enable_amazon_eks_kube_proxy         = true
  # enable_amazon_eks_vpc_cni           = true
  enable_aws_efs_csi_driver           = true
  enable_aws_load_balancer_controller = true

  # Self-managed Add-ons
  enable_ingress_nginx  = true
  enable_metrics_server = true
  enable_cert_manager   = true
  # enable_keda           = true
  # # Optional Map value; pass keda-values.yaml from consumer module
  # keda_helm_config = {
  #   name       = "keda"                              # (Required) Release name.
  #   repository = "https://kedacore.github.io/charts" # (Optional) Repository URL where to locate the requested chart.
  #   chart      = "keda"                              # (Required) Chart name to be installed.
  #   version    = ""                                  # (Optional) Specify the exact chart version to install. If this is not specified, it defaults to the version set within default_helm_config: https://github.com/aws-ia/terraform-aws-eks-blueprints/blob/main/modules/kubernetes-addons/keda/locals.tf
  #   namespace  = "keda"                              # (Optional) The namespace to install the release into.
  #   values     = [templatefile("${path.module}/keda-values.yaml", {})]
  # }

  # enable_kube_prometheus_stack = true
  # karpenter = {
  #   repository_username = data.aws_ecrpublic_authorization_token.token.user_name
  #   repository_password = data.aws_ecrpublic_authorization_token.token.password
  #   name                = "karpenter"
  #   chart               = "karpenter"
  #   repository          = "oci://public.ecr.aws/karpenter"
  #   version             = "v0.29.0"
  #   namespace           = "karpenter"
  # }

  # karpenter_helm_config = {
  #   name       = "karpenter"
  #   chart      = "karpenter"
  #   repository = "https://charts.karpenter.sh"
  #   version    = ""
  #   namespace  = "karpenter"
  # set = concat([{
  #     name  = "replicas"
  #     value = "1"
  #     type  = "auto"
  #     },
  #     {
  #       name  = "controller.resources.requests.cpu"
  #       value = "50m"
  #       type  = "string"
  #     },
  #     {
  #       name  = "controller.resources.limits.cpu"
  #       value = "100m"
  #       type  = "string"
  #     }],
  # }
  #Optional
  #   amazon_eks_vpc_cni_config = {
  #     addon_name = "vpc-cni"
  #     #addon_version            = "v1.11.2-eksbuild.1"
  #     service_account          = "aws-node"
  #     resolve_conflicts        = "OVERWRITE"
  #     namespace                = "kube-system"
  #     service_account_role_arn = ""
  #     preserve                 = true
  #     additional_iam_policies  = []
  #     configuration_values = jsonencode({
  #       env = {
  #         ENABLE_PREFIX_DELEGATION = "true"
  #         WARM_PREFIX_TARGET       = "1"
  #         WARM_IP_TARGET           = "5"
  #         MINIMUM_IP_TARGET        = "2"
  #       }
  #     })
  #     tags = {}
  #   }
  tags = local.tags
}

################################################################################
# Supporting Resources
################################################################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 5.0"

  name = local.name
  cidr = local.vpc_cidr

  azs             = local.azs
  private_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 4, k)]
  public_subnets  = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 48)]

  enable_nat_gateway = true
  single_nat_gateway = true

  public_subnet_tags = {
    "kubernetes.io/role/elb" = 1
  }

  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = 1
    # Tags subnets for Karpenter auto-discovery
    "karpenter.sh/discovery" = local.name
  }

  tags = local.tags
}
