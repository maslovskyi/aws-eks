terraform {
  required_version = ">= 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.47"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.9"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.20"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.5"
    }
    bcrypt = {
      source  = "viktorradnai/bcrypt"
      version = ">= 0.1.2"
    }
  }

  backend "s3" {
    bucket = "tf-s3-backend-1133"
    key    = "statefiles/terraform.tfstate"
    region = "us-east-1"

    dynamodb_table = "terraform-state-lock-table"
    encrypt        = true
  }
}
