module "kubernetes_addons" {
  source = "github.com/aws-ia/terraform-aws-eks-blueprints//modules/kubernetes-addons?ref=v4.32.1"
  # source = "github.com/aws-ia/terraform-aws-eks-blueprints//modules/kubernetes-addons"

  eks_cluster_id = module.eks_blueprints.eks_cluster_id

  # EKS Add-ons
  enable_amazon_eks_aws_ebs_csi_driver = true
  enable_amazon_eks_vpc_cni            = true
  enable_amazon_eks_coredns            = true
  enable_amazon_eks_kube_proxy         = true
  # Self-managed Add-ons
  enable_aws_efs_csi_driver = true

  # Optional aws_efs_csi_driver_helm_config
  # aws_efs_csi_driver_helm_config = {
  #   repository = "https://kubernetes-sigs.github.io/aws-efs-csi-driver/"
  #   version    = "2.4.0"
  #   namespace  = "kube-system"
  # }

  enable_aws_load_balancer_controller = true
  enable_kube_prometheus_stack        = true
  enable_metrics_server               = true
  enable_cert_manager                 = true

  # enable_cluster_autoscaler = true

  enable_karpenter = true
  karpenter_helm_config = {
    name       = "karpenter"
    chart      = "karpenter"
    repository = "oci://public.ecr.aws/karpenter"
    version    = "v0.29.0"
    namespace  = "karpenter"
  }

  #Optional
  amazon_eks_vpc_cni_config = {
    addon_name = "vpc-cni"
    #addon_version            = "v1.11.2-eksbuild.1"
    service_account          = "aws-node"
    resolve_conflicts        = "OVERWRITE"
    namespace                = "kube-system"
    service_account_role_arn = ""
    preserve                 = true
    additional_iam_policies  = []
    configuration_values = jsonencode({
      env = {
        ENABLE_PREFIX_DELEGATION = "true"
        WARM_PREFIX_TARGET       = "1"
        WARM_IP_TARGET           = "5"
        MINIMUM_IP_TARGET        = "2"
      }
    })
    tags = {}
  }
}

provider "helm" {
  kubernetes {
    host                   = module.eks_blueprints.eks_cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks_blueprints.eks_cluster_certificate_authority_data)

    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "aws"
      args        = ["eks", "get-token", "--cluster-name", module.eks_blueprints.eks_cluster_id]
    }
  }
}

