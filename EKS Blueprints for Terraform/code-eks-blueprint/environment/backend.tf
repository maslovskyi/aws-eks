terraform {
  backend "s3" {
    bucket = "tf-s3-backend-1133"
    key    = "statefiles/terraform.tfstate"
    region = "us-east-1"

    dynamodb_table = "terraform-state-lock-table"
    encrypt        = true
  }
}
